@extends("frontend.blog")
@section("content")

<div class="row blog-masonry-2col">
	@foreach($latestnews as $i => $topnews)
	<!-- Blog Item -->
	<div class="col-md-6 col-sm-6 col-xs-12 blog-masonry-item mb25">
		<div class="blog-one">
			<div class="blog-one-header">
				<a href="{{URL::to('news/'.$topnews->slug)}}">
					<img src="{{ URL::asset('upload/news/'.$topnews->image.'-b.jpg') }}" class="img-responsive" alt="image blog">
					<h3 class="pr25 pl25 color-dark" ><b>{{$topnews->title}}</b></h3>
				</a>
			</div>
			<div class="blog-one-body ">
				<p class="pb-50">
					{{ substr(strip_tags($topnews->description),0,200) }}..
				</p>
			</div>
			<div class="blog-one-footer">
				<small class="strong">
					<a class="text-primary">{{ date('d F Y',strtotime($topnews->created_at)) }}</a>
					<span class="text-primary">|</span>
					<a class="text-primary">{{$topnews->category_name}}</a>
				</small>
			</div>
		</div>
	</div>
	@endforeach
</div>

<!-- PAGGING STARTS -->
<div class="row pagging">
	<div class="col-lg-8 col-md-8">
		@include('_particles.pagination', ['paginator' => $latestnews])
	</div>
</div>
<!-- PAGGING ENDS -->
@endsection
