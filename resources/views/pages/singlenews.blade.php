@extends("frontend.blog")

@section('head_title', $news->title .' | '.getcong('site_name') )
@section('head_description', substr(strip_tags($news->description),0,200))
@section('head_image', asset('/upload/news/'.$news->image.'-b.jpg'))
@section('head_url', Request::url())

@section("content")
	<div class="blog-three-mini">
		<img src="{{ URL::asset('upload/news/'.$news->image.'-b.jpg') }}" alt="{{$news->image_title}}" class="img-responsive" style="width:100%">
		{{-- @if($news->image_title)<h4 class="overlay-category">{{$news->image_title}}</h4>@endif --}}

		<div class="bg-light p25">
			<h2 class="mb25 color-green text-left"><b>{{$news->title}}</b></h2>

			

			
			<h5 class="mb25 color-green">
				{{date('d F Y',strtotime($news->created_at))}} &nbsp;|&nbsp; 
				{{-- <i class="fa fa-eye"></i>{{$news->views}} |  --}}
				{{App\Categories::getCategoryInfo($news->cat_id)->category_name}}
				{{--  <i class="fa fa-pencil"></i>{{App\User::getUserInfo($news->user_id)->name}} |   --}}

				<!-- Share this post starts -->
				<div class="sharepost pull-right">			
					<a href="https://www.facebook.com/sharer.php?u={{URL::to('news/'.$news->slug)}}" class="btn btn-circle btn-primary" target="_blank">
						<i class="fa fa-facebook"></i>
					</a>
					<a href="https://twitter.com/share?url={{URL::to('news/'.$news->slug)}}" class="btn btn-circle btn-info" target="_blank">
						<i class="fa fa-twitter"></i>
					</a>
					<a href="https://www.linkedin.com/shareArticle?mini=true&url={{URL::to('news/'.$news->slug)}}" class="btn btn-circle bg-blue color-light" target="_blank">
						<i class="fa fa-linkedin"></i>
					</a>
				</div>
				<!-- Share this post ends -->
			</h5>

			
			
			<div class="lead">
				{!! $news->description !!}
			</div>
			
			
			{{-- <div class="blog-post-read-tag mt50">
				<i class="fa fa-folder"></i> Category:
				<a href="{{URL::to('/'.App\Categories::getCategoryInfo($news->cat_id)->category_slug)}}">{{App\Categories::getCategoryInfo($news->cat_id)->category_name}}</a>
			</div> --}}
			
			<?php $tags = explode(",", $news->tags);?>
			@if($tags)
			<div class="blog-post-read-tag">
				{{-- <i class="fa fa-tags"></i> Tags: --}}
				@foreach($tags as $tag)
				<a href="{{URL::to('tags?tags='.$tag)}}" class="text-primary"> {{$tag}}</a>,
				@endforeach
			</div>
			@endif 
			
			{{-- <div class="blog-post-read-tag">
				<i class="fa fa-flag"></i> Source:
				<a href="{{$news->source}}"> {{$news->source}}</a>,
			</div> --}}
		</div>


		<div class="row">
			<div class="col-md-12 ">
				<h3 class=" pt25 pb10 text-primary">Related Articles</h3>
			</div>
	
			<!--Blog Post -->
			@foreach($relatednews as $i => $topnews)
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="blog-one">
					<div class="blog-one-header">
						<a href="{{URL::to('news/'.$topnews->slug)}}">
							<img src="{{ URL::asset('upload/news/'.$topnews->image.'-b.jpg') }}" class="img-responsive" alt="image blog">
							<h3 class="pr25 pl25 text-primary" ><b>{{$topnews->title}}</b></h3>
						</a>
					</div>
					<div class="blog-one-body ">
						<p class="pb-50">
							{{ substr(strip_tags($topnews->description),0,200) }}..
						</p>
					</div>
					<div class="blog-one-footer ">
						<small class="strong">
							<a class="text-primary">{{ date('d F Y',strtotime($topnews->created_at)) }}</a>
							<span class="text-primary">|</span>
							<a class="text-primary">{{$topnews->category_name}}</a>
						</small>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
	

	
	
	
	{{-- <div class="blog-post-author mb50 pt30 bt-solid-1">
		<img src="{{ URL::asset('upload/members/'.App\User::getUserInfo($news->user_id)->image_icon.'-s.jpg') }}" class="img-circle" alt="{{App\User::getUserInfo($topnews->user_id)->name}}">
		<span class="blog-post-author-name">{{App\User::getUserInfo($news->user_id)->name}}</span></a>
	</div> --}}
	



	<!-- Leave a Comment Starts -->
	<div class="leave-comment">
		{{-- <div class="row category-caption">
			<div class="col-lg-12">
				<h2 class="pull-left">LEAVE A COMMENT</h2>
			</div>
		</div> --}}
		@if(getcong('disqus_comment_code'))
		<div class="row col-lg-12 col-md-12">
				{!! getcong('disqus_comment_code')!!}
		</div>
		@endif
		
		@if(getcong('facebook_comment_code'))
		<div class="row col-lg-12 col-md-12">
			
				{!! getcong('facebook_comment_code')!!}
				<h3>Facebook Conversations</h3> 
				<div class="fb-comments" data-href="{{  Request::url() }}" data-numposts="5" data-width="100%" style="width: 100%"></div>
		</div>
		@endif
	</div>
	<!-- Leave a Comment Ends -->
 
@endsection
