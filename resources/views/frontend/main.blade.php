<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>MONEX AUSTRALIA</title>
    <meta name="keywords" content="share,broker,with,access,international,cost,nbsp,account,listed,securities,australia,asia,find,million,worldwide,over,billion,assets,more,monex,place,market,fair,price,global,opportunities,manage,clients,execution,system,allows,give,world,single,trading,platform,diversify,portfolio,open,what,excellent,online,trade,real,time,exchanges,industry,superb,experience,service,competitive,pricing,means,investing,multi,stock,across,value,powerful,order,types,simply,watchlist,investments,updates,business,group,brokerage" />
    <meta name="description" content="Your new share broker with a difference Ready for access to international markets a low cost? You&#039;re…" />
    <meta name="dcterms.rights" content="MONEX AUSTRALIA &copy; All Rights Reserved" >
    <meta name="robots" content="index" />
    <meta name="robots" content="follow" />
    <meta name="revisit-after" content="1 day" />
    <meta name="generator" content="Powered by CMS pro! v4.00" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" type="image/x-icon" href="http://localhost:8888/monex2/assets/favicon.ico" />
		
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/core/bootstrap.min.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/core/animate.min.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/main/main.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/main/setting.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/main/hover.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/magnific/magic.min.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/magnific/magnific-popup.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/magnific/magnific-popup-zoom-gallery.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/color/green.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/icon/font-awesome.css">
    <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/icon/et-line-font.css">
    <!-- <link rel="stylesheet" href="http://localhost:8888/monex2/theme/freedomcfd/assets/css/main/app.css">  -->


    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.css">
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <style type="text/css">
        .register-modal{
            width: 350px;
            top:100px;
        }

        .nav-boderno{
            border-bottom: 0px !important;

        }
    </style>
    
    <script type="text/javascript">
        var SITEURL = "http://localhost:8888/monex2";
    </script>


    {!! getcong('site_header_code') !!}
	
	{!! getcong('addthis_share_code')!!}

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5DWQBSC');</script>
    <!-- End Google Tag Manager -->

</head>
<body  id="page-top" data-spy="scroll" data-target=".navbar" data-offset="100">

    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DWQBSC" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <a href="#page-top" class="go-to-top">
        <i class="fa fa-long-arrow-up"></i>
    </a>


    <!-- Navigation Area
    ===================================== -->
    <nav class="navbar navbar-fixed-top">
        <div class="top-navbar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list list-inline">
                            <li>
					            <small>Welcome: Guest!</small>
                            </li>
                        </ul>
                        <ul class="list list-inline">
                            <li><a href="http://localhost:8888/monex2/page/our-contact-info"><i class="fa fa-phone text-primary mr5" aria-hidden="true"></i>CONTACT US</a></li>
                            <!-- <li><a href="#">LOGOUT</a></li> -->
                            <li><a href="http://localhost:8888/monex2/page/before-register"  class="text-primary">SIGN UP</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="list list-inline top-navbar-right">
                            <li>
                                <a href="#">
                                    <div class="list-icon">
                                        <img src="http://localhost:8888/monex2/theme/freedomcfd/assets/media/icons/uk-flag.png" alt="UK">
                                    </div>
                                    <small>LANGUAGE</small>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-nav">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="http://localhost:8888/monex2">
                                <img src="http://localhost:8888/monex2/uploads/monex-b.png" alt="logo" width="150px">
                            </a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse navbar-right">
                        <ul class="nav navbar-nav" id="menu">
                        <li class="nav-item"><a href="http://localhost:8888/monex2/page//" class="normal"><strong>test</strong><small>test</small></a></li>
                        <li class="nav-item"><a href="http://localhost:8888/monex2/page/home/" class="active"><strong>Home</strong></a></li>
                        <li class="nav-item"><a href="http://localhost:8888/monex2/page/why-monex/" class="normal"><strong>Why Monex</strong><small>Why Monex desc</small></a></li>
                        <li class="nav-item"><a href="http://localhost:8888/monex2/page/fee/" class="normal"><strong>Fees</strong></a></li>
                        <li class="nav-item" data-cols=" two cols"><a href="http://localhost:8888/monex2/page/about-monex/" class="normal"><strong>About Us</strong></a></li>
                        <li class="nav-item"><a href="http://localhost:8888/monex2/page/support/" class="normal"><strong>Support</strong></a></li>
                        <li class="nav-item"><a href="http://localhost:8888/monex2/page/testnew/" class="normal"><strong>Blog</strong></a></li>
                        <li class="nav-item"><a href="http://localhost:8888/monex2/page/our-contact-info/" class="normal"><strong>Contact Us</strong></a></li>
                        <li class="nav-item"><a href="http://localhost:8888/monex2/page/login/" class="normal"><strong>Login</strong></a></li>
                    </ul>

                    <!--<ul class="nav navbar-nav">
                        <li class="hidden"><a href="#page-top"></a></li>
                        <li><a href="http://localhost:8888/monex2">HOME</a></li>
                        <li><a href="#portfolioGrid">WHY MONEX</a></li>
                        <li><a href="#service">BROKERAGE FEES</a></li>
                        <li><a href="http://localhost:8888/monex2/page/about-freedomcfd">ABOUT US</a></li>
                        <li><a href="http://localhost:8888/monex2/page/our-contact-info">CONTACT US</a></li>
                        <li><a href="http://localhost:8888/monex2/page/login"><i class="fa fa-lock fa-fw"></i>Login</a></li>
                    </ul>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </nav>


    <!-- Full Layout -->
    <div id="page" class="bg-gray pt100 pb100" style="padding-bottom:0px;">
        <div class="prolific-grid">
            <div class="prolific-content-full clearfix">
                <header id="info-1" class="pb40" data-parallax="scroll" data-speed="0.5" style="background-image: url('https://www.monexsecurities.com.au/uploads/images/design/bg2.png');background-size:cover;">
                    <div class="container">
                        <div class="pt35">
                            <div class="col-md-12 text-left">
                                <h1 class="color-light"> Blog</h1>
                            </div>
                        </div>
                    </div>
                </header>


                @yield("content")
                
                <div class="pb40 parallax-25" id="info-1" style="background-image: url('http://localhost/monex2/uploads/images/design/bg2.png');background-size:cover;">
                    <div class="container">
                        <div class="row pt40">
                            <div class="col-md-12 text-center">
                                <h4 class="color-light">				
                                    Welcome to MONEX, the place where smart money people can find access to a truly<br />
                                    international equities market at a fair price, to help unlock global investment opportunities 				
                                    <br />
                                    and manage risk through diversification to grow their wealth. 				
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pt100 pb100" style="background: url('http://localhost/monex2/uploads/images/design/bg3.png') 50% 30% no-repeat; background-size: cover;">
                    <div class="container">
                        <div class="row">
                            <!-- left content start -->
                            <div class="col-md-6 pb100">
                                <div class="general-content">
                                    <h1 class="mb25 color-green">					Who are our clients? 					</h1>
                                    <h4>Our clients requiring global access to shares at a low cost include:</h4>
                                    <h4><i class="fa fa-angle-right color-green pr20"></i> Existing &amp; Professional Traders</h4>
                                    <h4><i class="fa fa-angle-right color-green pr20"></i> Institutional Partners - Asset Managers &amp; Brokers</h4>
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </div>
                            <!-- left content end -->
                        </div>
                    </div>
                </div>
            
                <div class="parallax-window-3" id="general-content-1" style="background-image: url('http://localhost/monex2/uploads/images/design/bg4.png');background-size:cover;">
                    <div class="container pb100">
                        <div class="row">
                            <!-- left content start -->
                            <div class="col-md-6">
                            </div>
                            <!-- left content end -->
                            <!-- right content start -->
                            <div class="col-md-5 col-md-push-1 pt100 animated" data-animation-delay="150" data-animation="fadeInUp">
                                <div class="general-content">
                                    <h1 class="mb25">How Do We Do It?</h1>
                                    <h5 style="text-indent:-20px;margin-left:24px;"><i class="fa fa-angle-right color-green pr20"></i>By leveraging the accumulated strengths of our global network of companies - Monex, Inc. (Japan), TradeStation Securities (USA) and Monex BOOM (Hong Kong), we are able to access very low execution costs and a bespoke back-end system that reduces costs and allows us to pass the savings along to you.</h5>
                                    <h5 style="text-indent:-20px;margin-left:24px;"><i class="fa fa-angle-right color-green pr20"></i>We created Monex Australia to fill a gap in the market that clearly needed to be filled. We give you access to a world of more than 50,000 shares and listed securities that are otherwise hard to reach from a single, fair price trading platform. 					</h5>
                                </div>
                            </div>
                            <!-- right content end -->
                        </div>
                    </div>
                    <div class="row pt25" style="background-color: rgba(66, 110, 107, 0.4);">
                        <div class="container">
                            <div class="col-md-6 text-center animated" data-animation-delay="200" data-animation="fadeInLeft">
                                <h1 class="color-light">Diversify your portfolio today</h1>
                            </div>
                            <div class="col-md-6">
                                <p class="intro-text-big text-center color-light pt25 pb25 animated" data-animation-delay="200" data-animation="fadeInRight">
                                    <a class="button button-lg button-circle button-green button-md" href="http://localhost/monex2/page/our-contact-info/" target="_blank">OPEN AN ACCOUNT</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray pt50 pb100">
                    <div class="container">
                        <div class="row">
                            <h1 class="text-center pb50 color-green">What our clients are saying 			</h1>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12 hover-wobble-vertical">
                                <div class="team team-one" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                                    <img class="img-responsive img-circle" src="http://localhost/monex2/uploads/images/design/profile1.png">
                                    <h4 class="color-green">One excellent broker</h4>
                                    <h5 class="m25">Monex – has an online platform that allows you to trade most Asian markets seamlessly in real time, right from a computer&nbsp;&nbsp;...through a multicurrency account, giving you the best prices and execution in real time on these exchanges.</h5>
                                    <small>Karim</small>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 hover-wobble-vertical">
                                <div class="team team-one" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                                    <img class="img-responsive" src="http://localhost/monex2/uploads/images/design/profile2.png">
                                    <h4 class="color-green">Excellent</h4>
                                    <h5 class="m25">I have to say that every single interaction with this platform is just a pleasure - I really appreciated your flexibility and your focus on getting things done in the most customer friendly manner!!!! you are quite a role model for the financial services industry!!&nbsp;</h5>
                                    <small>Tom</small>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 hover-wobble-vertical">
                                <div class="team team-one" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.1), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                                    <img class="img-responsive" src="http://localhost/monex2/uploads/images/design/profile3.png">
                                    <h4 class="color-green">Superb</h4>
                                    <h5 class="m25">I have experience of 3 other platforms. One was good, one mediocre, and one terrible! This platform is far and away the easiest, simplest to understand, and the very best for efficiency and service: superb.&nbsp;</h5>
                                    <small class="pt25">Janine</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pt100 pb100 bg-black">
                    <div class="container">
                        <div class="row">
                            <h2 class="text-center color-light">Super Competitive Pricing</h2>
                            <img src="http://localhost/monex2/uploads/images/design/bg5.png" width="100%">
                        </div>
                    </div>
                </div>
                <div class="pt50 pb50 bg-white">
                    <div class="container">
                        <div class="row">
                            <h1 class="text-center color-green pb50 animated" data-animation-delay="100" data-animation="fadeInUp">If you have only invested in Australia then your assets are in a&nbsp;basket of less than 3% of international market capitalisation.</h1>
                            <h4 class="text-center animated color-green" data-animation-delay="100" data-animation="fadeInUp">This means you are missing out on 97% of possible investing opportunities in the world. The proof:</h4>
                            <img class="img-responsive pr50 pl50 pb25" src="http://localhost/monex2/uploads/images/design/chart.png" width="100%">
                            <h4 class="text-center animated" data-animation-delay="100" data-animation="fadeInUp">Source:&nbsp;Based on Vanguard Research 2016</h4>
                            <p class="intro-text-big text-center color-light pt25 pb25 animated" data-animation-delay="150" data-animation="fadeInUp">
                                <a class="button button-lg button-circle button-green button-md" href="http://localhost/monex2/page/our-contact-info/" target="_blank">OPEN AN ACCOUNT</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pt100 pb100 bg-gray">
                    <div class="container">
                        <div class="row mb40 pb20">
                            <h1 class="text-center color-green animated" data-animation-delay="100" data-animation="fadeInUp">Why trade with Monex?</h1>
                            <h3 class="text-center">When you trade with Monex, you get all this and much more...</h3>
                        </div>
                        <div class="row">
                            <!-- Content Box Center Icon Circle with Background-->
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="content-box content-box-center content-box-icon-circle">
                                    <img src="http://localhost/monex2/uploads/images/design/icon1.png" width="120">
                                    <h4 class="color-green">Multi-Market<br />
                                                                                                                    Stock Trading
                                    </h4>
                                    <h4 class="color-green">
                                    <p>
                                        We give access to exchange listed securities in 12 Markets across Asia and the United States, which covers around 70% of world market value.
                                    </p>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="content-box content-box-center content-box-icon-circle">
                                    <img src="http://localhost/monex2/uploads/images/design/icon2.png" width="120">
                                    <h4 class="color-green">Competitive<br />
                                                                                                                    Pricing 					
                                    </h4>
                                    <p class="pr10 pl10">
                                                                                                                                        You can trade a wide range of markets&nbsp;at a low cost.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="content-box content-box-center content-box-icon-circle">
                                    <img src="http://localhost/monex2/uploads/images/design/icon3.png" width="120">
                                    <h4 class="color-green">Powerful<br />
                                                                                                                    Order Types 					
                                    </h4>
                                    <p class="pr10 pl10">
                                                                                                                                        We provide a collection of powerful online trading order types to fit your trading strategy. Simply pre-set your orders and let our system do the work for you.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="content-box content-box-center content-box-icon-circle">
                                    <img src="http://localhost/monex2/uploads/images/design/icon5.png" width="120">
                                    <h4 class="color-green">Integrated Account<br />
                                    Management
                                    </h4>
                                    <p class="pr10 pl10">
                                        Manage your investments across markets under one account number.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="content-box content-box-center content-box-icon-circle">
                                    <img src="http://localhost/monex2/uploads/images/design/icon7.png" width="120">
                                    <h4 class="color-green">Real-time<br />
                                    Account Updates
                                    </h4>
                                    <p class="pr10 pl10">
                                                                                                                                        Instant online updates for order status, portfolio holdings and funds available.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="content-box content-box-center content-box-icon-circle">
                                    <img src="http://localhost/monex2/uploads/images/design/icon6.png" width="120">
                                    <h4 class="color-green">Complete<br />
                                                                                                                    Web Access 					
                                    </h4>
                                    <p class="pr10 pl10">
                                                                                                                                        Simply place all instructions over an Internet browser. No need to download any software&nbsp;or apps.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="content-box content-box-center content-box-icon-circle">
                                    <img src="http://localhost/monex2/uploads/images/design/icon4.png" width="120">
                                    <h4 class="color-green">Global Institutional<br />
                                    Footprint
                                    </h4>
                                    <p class="pr10 pl10">
                                        Monex Group is an online brokerage business with nearly 2 million clients and over USD 40 billion assets under custody as a group.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="content-box content-box-center content-box-icon-circle">
                                    <img src="http://localhost/monex2/uploads/images/design/icon8.png" width="120">
                                    <h4 class="color-green">B2B Solutions </h4>
                                    <p class="pr10 pl10">
                                                                                        Contact us to discuss the ways in which we can help you and your clients access offshore markets at a fair price.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pt50 pb50 bg-white">
                    <div class="container">
                        <div class="row">
                            <h1 class="text-center color-green animated" data-animation-delay="100" data-animation="fadeInUp">Who is Monex?</h1>
                            <h4 class="text-center animated" data-animation-delay="100" data-animation="fadeInUp">...and why should you trust us to do business with?</h4>
                            <img class="img-responsive" src="http://localhost/monex2/uploads/images/design/map.png" width="100%">
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">
                                    <i class="fa fa-angle-right color-green"></i>
                                    <p>
                                                                                                                                        Monex Group is an online brokerage business with nearly 2 million clients worldwide with over USD 40 billion assets under custody as a group.
                                    </p>
                                </div>
                                <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">
                                    <i class="fa fa-angle-right color-green"></i>
                                    <p>
                                                                                                                                        Founded in 1999, the Monex Group is an online brokerage business with over 830 employees through 12 offices worldwide, who are passionate about one thing only – to give our customers unmatched access to international markets at a reasonable cost.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">
                                    <i class="fa fa-angle-right color-green"></i>
                                    <p>
                                                                                                                                        Reliability and ‘Above all Integrity’ in everything we do. This simply means that we say what we do and do what we say.
                                    </p>
                                </div>
                                <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">
                                    <i class="fa fa-angle-right color-green"></i>
                                    <p>
                                                                                                                                        Monex offers you cutting edge industry first digital applications and technology that allow you to trade in a way that is streamlined for you and your personal trading devices.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="content-box content-box-icon content-box-left-icon" style="margin-top: 15px">
                                    <i class="fa fa-angle-right color-green"></i>
                                    <p>
                                                                                                                                        Quality service and client satisfaction are inherent in all that we do. Our service commitment delivers on the promise of a private, secure, fast and inexpensive trading experience.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pb40 parallax-25" id="find-out-more" style="background-image: url('http://localhost/monex2/uploads/images/design/bg2.png');background-size:cover;">
                    <div class="container">
                        <div class="row pt40">
                            <div class="col-md-12 text-center">
                                <h2 class="color-light">				Yes I want to diversify my portfolio 				</h2>
                                <h3 class="color-dask">				Find out how easy it is to start investing in international stocks at a low cost.</h3>
                            </div>
                            <form id="prolific_formcontactmonex" name="prolific_formcontactmonex" method="post" class="form-horizontal" >
                                <input name="action" value="sendmail_monex" type="hidden">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="col-md-6 pt25">
                                        <input class="form-control form-rounded-green" name="firstname" placeholder="First Name*" value="">
                                    </div>
                                    <div class="col-md-6 pt25">
                                        <input class="form-control form-rounded-green" name="lastname" placeholder="Last Name" value="">
                                    </div>
                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="col-md-6 pt25">
                                        <input class="form-control form-rounded-green" name="email" placeholder="Email*" value="">
                                    </div>
                                    <div class="col-md-6 pt25">
                                        <input class="form-control form-rounded-green" name="phonemobile" placeholder="Phone Number" value="">
                                    </div>
                                </div>
                                <div class="col-md-12 text-center pt50" id="btn_submit_contactmonex" style="display:none;">
                                    <button id="btnmonex" class="button-o button-light button-lg button-circle">Yes, I want to know more</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

  
  
    <!-- Bottom Widgets -->
    <div id="botwidget">
        <div class="prolific-grid">
            <section class="botwidget clearfix">
                <div class="columns">
                    <div class="screen-100 phone-100">
                        <div class="botwidget-wrap add menu footer">
                            <div class="widget-body">
                                <div id="footer" class="footer-two pt50 bg-dark">
                                    <div class="container-fluid">
                                        <div class="container">
                                            <div class="row">
                                                <div class="clearfix bb-solid-1 pb35">
                                                    <!-- footer about start -->
                                                    <div class="col-md-6 col-xs-12">
                                                        <div class="footer-brand">
                                                            <img src="http://localhost/monex2/theme/freedomcfd/assets/media/logo/logo-monex-w.png" alt="logo" width="150px">
                                                        </div>
                                                        <button class="button-o button-green button-circle"><a href="http://localhost/monex2/page/our-contact-info/" target="_blank"><h7 class="color-green">OPEN AN ACCOUNT</h7></a></button>
                                                    </div>
                                                    <!-- footer about end -->
                                                    <!-- footer about start -->
                                                    <div class="col-md-3 col-xs-12">
                                                        <ul class="list-caret">
                                                            <li><a href="http://localhost/monex2/page/why-monex/">Why Monex</a></li>
                                                            <li><a href="" target="_blank">Fees</a></li>
                                                            <li><a href="http://localhost/monex2/page/support/" target="_blank">Support</a></li>
                                                            <li><a href="http://localhost/monex2/page/faq/" target="_blank">FAQ</a></li>
                                                            <li><a href="http://localhost/monex2/page/about-monex/">About Us</a></li>
                                                            <li><a href="http://localhost/monex2/page/our-contact-info/">Contact Us</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- footer about end -->
                                                    <!-- footer about start -->
                                                    <div class="col-md-3 col-xs-12">
                                                        <ul class="list-caret">
                                                            <li><a href="http://localhost/monex2/uploads/Terms-and-Conditions.pdf" target="_blank">Terms and Conditions</a></li>
                                                            <li><a href="http://localhost/monex2/uploads/Financial-Services-Guide.pdf" target="_blank">Financial Services Guide</a></li>
                                                            <li><a href="" target="_blank">Fees and Charges Schedules</a></li>
                                                            <li><a href="http://localhost/monex2/uploads/Privacy-Policy.pdf" target="_blank">Privacy Policy</a></li>
                                                            <li><a href="http://localhost/monex2/page/complaints/" target="_blank">Complaint Handling Policy</a></li>
                                                        </ul>
                                                    </div>
                                                    <!-- footer about end -->
                                                </div>
                                            </div>
                                            <!-- row end -->
                                        </div>
                                        <!-- container end -->
                                    </div>

                                    <!-- container-fluid end -->
                                    <div class="container-fluid pt20">
                                        <div class="container">
                                            <div class="row">
                                                <!-- copyright start -->
                                                <div class="col-md-6 col-sm-6 col-xs-6 pull-left">
                                                    <p>
                                                        &copy; Monex Securities Australia Pty Ltd 2017
                                                    </p>
                                                </div>
                                                <!-- copyright end -->
                                                <!-- footer bottom start -->
                                                <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
                                                    <p class="text-right">
                                                        <a href="https://www.facebook.com/MonexAustralia" "=" " target="_blank " class="mr20 "><i class="fa fa-facebook
                                                            " aria-hidden="true "></i></a><a href="https://twitter.com/MonexAustralia "  target="_blank " class="mr20
                                                            "><i class="fa fa-twitter " aria-hidden="true "></i></a><a href="https://www.linkedin.com/company/18164522/ " "=""
                                                            target="_blank" class="mr50">
                                                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                        </a>
                                                    </p>
                                                </div>
                                                <!-- footer bottom end -->
                                                <div class="col-sm-12">
                                                    <p>
                                                        This Internet website (
                                                        <a href="http://monexsecurities.com.au/">monexsecurities.com.au</a>) and information and services referred to in the ‘website’ are directed to
                                                        and available for Australian residents. Our website is issued and made available in Australia through
                                                        Monex Securities Australia Pty Ltd ABN 84 142 210 179 and AFSL No. 363972 (‘Monex AU’, “us’ ‘we’ ‘our’).
                                                        &nbsp;
                                                    </p>
                                                    <p>
                                                        This website contains material in relation to the Financial Products and services available in Australia offered by Monex
                                                        AU. By accessing this website you agree to be bound by our Terms and Conditions. These terms and conditions
                                                        are subject to change without prior notice. It is your responsibility to review for any amendments of
                                                        our terms and conditions that may occur from time to time published on our website. Do not access this
                                                        website if you do not agree with these terms and conditions set out herein.
                                                    </p>
                                                    <p>
                                                        The information, content and material on this website is for information purposes only. It is believed to be reliable at
                                                        the time of publication however Monex AU does not warrant its completeness, timeliness or accuracy. The
                                                        information and materials contained in this website are subject to change without notice.
                                                    </p>
                                                    <p>
                                                        This website is given for general information only. As general information, no consideration or evaluation is given of the
                                                        investment objectives or financial situation of any particular person. Trading and investing involves
                                                        substantial financial risk. All customers of Monex AU should make their own evaluation of the merits
                                                        and suitability of any financial products and/or advice or seek specific personal advice as to the appropriateness
                                                        of engaging in any activity referred to in this website in light of their own particular financial circumstances
                                                        and objectives.
                                                    </p>
                                                    <p>
                                                        Please&nbsp;
                                                        <a href="http://localhost/monex2/page/disclaimer-details/" target="_blank">click here</a> to read our full disclaimer.
                                                    </p>
                                                </div>
                                                <!-- row end -->
                                            </div>
                                            <!-- container end -->
                                        </div>
                                        <!-- container-fluid end -->
                                    </div>
                                    <div>
                                </div>
                                </div>
                            </div>
        
                        </div>

                    </div>
                </div>

            </section>
        </div>
    </div>

    <!-- Bottom Widgets /-->
    <!-- Full Layout /-->

    <!-- footer Area
    ===================================== -->
    
    
    <!-- JQuery Core
    =====================================-->
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/core/jquery.min.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/core/bootstrap.min.js"></script>
    
    <!-- Magnific Popup
    =====================================-->
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>
    
    <!-- Progress Bars
    =====================================-->
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/progress-bar/bootstrap-progressbar.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>        

    <!-- Text Rotator
    =====================================-->
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/text-rotator/jquery.simple-text-rotator.min.js"></script>
    
    <!-- JQuery Main
    =====================================-->
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/main/jquery.appear.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/main/isotope.pkgd.min.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/main/parallax.min.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/main/jquery.countTo.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/main/owl.carousel.min.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/main/jquery.sticky.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/main/imagesloaded.pkgd.min.js"></script>
    <script src="http://localhost:8888/monex2/theme/freedomcfd/assets/js/main/main.js"></script>

    <script src="http://localhost:8888/monex2/theme/freedomcfd/master.js"></script>
    <script src="http://localhost:8888/monex2/assets/global.js"></script>
    
</body>
</html>

