<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title>MONEX AUSTRALIA</title>
    <meta name="keywords" content="share,broker,with,access,international,cost,nbsp,account,listed,securities,australia,asia,find,million,worldwide,over,billion,assets,more,monex,place,market,fair,price,global,opportunities,manage,clients,execution,system,allows,give,world,single,trading,platform,diversify,portfolio,open,what,excellent,online,trade,real,time,exchanges,industry,superb,experience,service,competitive,pricing,means,investing,multi,stock,across,value,powerful,order,types,simply,watchlist,investments,updates,business,group,brokerage" />
    <meta name="description" content="Your new share broker with a difference Ready for access to international markets a low cost? You&#039;re�" />
    <meta name="dcterms.rights" content="MONEX AUSTRALIA &copy; All Rights Reserved" >
    <meta name="robots" content="index" />
    <meta name="robots" content="follow" />
    <meta name="revisit-after" content="1 day" />
    <meta name="generator" content="Powered by CMS pro! v4.00" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" type="image/x-icon" href="{{ config('app.url') }}assets/favicon.ico" />

    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/core/bootstrap.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/core/animate.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/main/main.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/main/setting.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/main/hover.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/magnific/magic.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/magnific/magnific-popup.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/magnific/magnific-popup-zoom-gallery.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/color/green.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/icon/et-line-font.css">
    <!-- <link rel="stylesheet" href="{{ config('app.url') }}theme/freedomcfd/assets/css/main/app.css">  -->

    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/chosen/1.7.0/chosen.css">
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <style type="text/css">
        .register-modal{
            width: 350px;
            top:100px;
        }

        .nav-boderno{
            border-bottom: 0px !important;

        }
    </style>

    <script type="text/javascript">
        var SITEURL = "{{ env('APP_HOST') }}/monex2";
    </script>


    {!! getcong('site_header_code') !!}

	{!! getcong('addthis_share_code')!!}

</head>
<body  id="page-top" data-spy="scroll" data-target=".navbar" data-offset="100">

    <a href="#page-top" class="go-to-top">
        <i class="fa fa-long-arrow-up"></i>
    </a>


    <!-- Navigation Area
    ===================================== -->
    <nav class="navbar navbar-fixed-top">
        <div class="top-navbar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <ul class="list list-inline">
                            <li>
                                <small>Welcome: Guest!</small>
                            </li>
                        </ul>
                        <ul class="list list-inline">
                            <li>
                                <a href="https://www.monexsecurities.com.au/page/our-contact-info">
                                    <i class="fa fa-phone text-primary mr5" aria-hidden="true"></i>CONTACT US</a>
                            </li>
                            <!-- <li><a href="#">LOGOUT</a></li> -->
                            <li>
                                <a href="https://www.monexsecurities.com.au/page/before-register" class="text-primary">SIGN UP</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="list list-inline top-navbar-right">
                            <li>
                                <a href="#">
                                    <div class="list-icon">
                                        <img src="https://www.monexsecurities.com.au/theme/freedomcfd/assets/media/icons/uk-flag.png" alt="UK">
                                    </div>
                                    <small>LANGUAGE</small>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-nav">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="https://www.monexsecurities.com.au">
                                <img src="https://www.monexsecurities.com.au/uploads/monex-b.png" alt="logo" width="150px">
                            </a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse navbar-right">
                            <ul class="nav navbar-nav" id="menu" data-smartmenus-id="15224111097093308">
                                <li>
                                    <a href="https://www.monexsecurities.com.au/page/home/" class="normal">Home</a>
                                </li>
                                <li>
                                    <a href="https://www.monexsecurities.com.au/page/why-monex/" class="normal">Why Monex</a>
                                </li>
                                <li>
                                    <a href="https://www.monexsecurities.com.au/page/fee/" class="normal">Fees</a>
                                </li>
                                <li data-cols=" two cols">
                                    <a href="https://www.monexsecurities.com.au/page/about-monex/" class="normal">About Us</a>
                                </li>
                                <li>
                                    <a href="{{ url('/') }}" class="active">Monex News</a>
                                </li>
                                <li>
                                    <a href="https://www.monexsecurities.com.au/page/support/" class="normal">Support</a>
                                </li>
                                <li>
                                    <a href="https://www.monexsecurities.com.au/page/our-contact-info/" class="normal">Contact Us</a>
                                </li>
                                <li>
                                    <a href="https://www.monexsecurities.com.au/page/before-register/" class="normal">Open an Account</a>
                                </li>
                                <li>
                                    <a href="https://www.monexsecurities.com.au/page/login/" class="normal">Login</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </nav>


    <!-- Full Layout -->
    <div id="page" class="bg-gray pt100 pb100" style="padding-bottom:0px;">
        <div class="prolific-grid">
            <div class="prolific-content-full clearfix">
                <header id="info-1" class="pb40" data-parallax="scroll" data-speed="0.5" style="background-image: url('https://www.monexsecurities.com.au/uploads/images/design/bg2.png');background-size:cover;">
                    <div class="container">
                        <div class="pt35">
                            <div class="col-md-12 text-left">
                                <h1 class="color-light">Monex News</h1>
                            </div>
                        </div>
                    </div>
                </header>

                <div id="blog" class="pt40 pb50">
                    <div class="container">
                        <nav class="navbar navbar-default" style="background-color: transparent; border-top: 0px; border-left: 0px; border-right: 0px;">
                            <ul class="nav navbar-nav" style="padding: 0px;">
                                <?php $catname = isset($cat->category_name) ? $cat->category_name : ''; ?>
                                @foreach(\App\Categories::orderBy('category_name')->get() as $cat)
                                <li>
                                    <a href="{{URL::to($cat->category_slug)}}" class="{{ ($catname == $cat->category_name) ? "" : "color-dark2" }}"><b>{{$cat->category_name}}</b></a>
                                </li>
                                @endforeach
                            </ul>
                            <a href="{{ URL::to('/') }}" class="button button-md button-gray pull-right" type="button" name="button">VIEW ALL</a>
                        </nav>


                        <div class="row">
                            <div class="col-md-9 mt25">

                                @yield("content")

                            </div>

                            <!-- Blog Sidebar
				            ===================================== -->
							@include('_particles.sidebar')
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Bottom Widgets /-->
    <!-- Full Layout /-->

    <!-- footer Area
    ===================================== -->
    @include('_particles.footer')


    <!-- JQuery Core
    =====================================-->
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/core/jquery.min.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/core/bootstrap.min.js"></script>

    <!-- Magnific Popup
    =====================================-->
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/magnific-popup/magnific-popup-zoom-gallery.js"></script>

    <!-- Progress Bars
    =====================================-->
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/progress-bar/bootstrap-progressbar.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/progress-bar/bootstrap-progressbar-main.js"></script>

    <!-- Text Rotator
    =====================================-->
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/text-rotator/jquery.simple-text-rotator.min.js"></script>

    <!-- JQuery Main
    =====================================-->
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/main/jquery.appear.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/main/isotope.pkgd.min.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/main/parallax.min.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/main/jquery.countTo.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/main/owl.carousel.min.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/main/jquery.sticky.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/main/imagesloaded.pkgd.min.js"></script>
    <script src="{{ config('app.url') }}theme/freedomcfd/assets/js/main/main.js"></script>

    <script src="{{ config('app.url') }}theme/freedomcfd/master.js"></script>
    <script src="{{ config('app.url') }}assets/global.js"></script>

</body>
</html>
