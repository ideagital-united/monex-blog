<!-- HEADER STARTS
				========================================================================= -->
			<header>
				<!-- TOP ROW STARTS -->
				<div class="top-nav hidden-sm hidden-xs">
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<div id="date"></div>
						</div>
						<div class="col-lg-6 col-md-6">
							 
						</div>
					</div>
				</div>
				<!-- TOP ROW ENDS -->
				<!-- LOGO & ADS STARTS -->
				<div class="row">
					<div class="col-lg-4 col-md-2 logo"><a href="{{ URL::to('/') }}"><img src="{{ URL::asset('upload/'.getcong('site_logo')) }}" alt=""></a></div>
					<div class="col-lg-8 col-md-10">
						<div class="ad-728x90 visible-lg visible-md">
							
							@if(getcong_widgets('header_advertise'))
							{!!getcong_widgets('header_advertise')!!}
							
							@else
							
							<img src="{{ URL::asset('assets/images/ads/3d_728x90_v2.gif') }}" alt="">
							@endif
							 
							
						</div>						 
						
					</div>
				</div>
				<!-- LOGO & ADS ENDS -->
			</header>
			<!-- /. HEADER ENDS
				========================================================================= -->
