<?php
  /**
   * Index
   *
   * @package CMS Pro
   * @author prolificscripts.com
   * @copyright 2014
   * @version $Id: index.php, v4.00 2014-04-20 10:12:05 gewa Exp $
   */
    if (!defined('_VALID_PHP'))
        define("_VALID_PHP", true);
    require_once dirname(__FILE__)."/init.php";

  //$row = $content->renderPage(true);
  $plgresult = $content->getPluginLayoutFront();

  //$widgettop = Content::countPlace($plgresult, "top");
  $widgetbottom = Content::countPlace($plgresult, "bottom");
  //$widgetleft = Content::countPlace($plgresult, "left");
  //$widgetright = Content::countPlace($plgresult, "right");
  $assets = Content::countPlace($plgresult, false, false);

  //$totalleft = count($widgetleft);
  //$totalright = count($widgetright);
  //$totaltop = count($widgettop);
  $totalbot = count($widgetbottom);

  $cookie_value = 'MONEX';

?>

<!-- Bottom Widgets -->
  
  <div id="botwidget">
  
  <div class="prolific-grid">
  
  <?php include(THEMEDIR . "/bottom_widget.tpl.php");?>

  </div>

</div>

<!-- Bottom Widgets /-->
