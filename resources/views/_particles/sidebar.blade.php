<div class="col-md-3">

    <div id="sidebar" class="mt25 animated fadeInRight visible" data-animation="fadeInRight" data-animation-delay="250">
        <div class="panel">
            <!-- Search
                ===================================== -->
            <div class="pr25 pl25 clearfix">
                <h5 class="mt25 text-primary">
                    <b>Search</b>
                    <span class="heading-divider mt10"></span>
                </h5>
                {!! Form::open(array('url' => array('search'),'role'=>'form','method'=>'get')) !!}
                <div class="form-group bg-gray ">
                    <input type="text" name="q" class="form-control bg-gray" placeholder="Search" value="{{ request('q') }}">
                </div>
                {!! Form::close() !!}
            </div>




            <div class="pr25 pl25 clearfix">
                <h5 class="mt25 text-primary">
                    <b>Popular Articles</b>
                    <span class="heading-divider mt10"></span>
                </h5>

                @foreach(\App\News::where('status','1')->orderBy('views','desc')->take(getcong_widgets('popular_posts_limit'))->get() as
                $popular_news)
                <div class="content-box content-box-left">

                    <a class="text-primary" href="{{URL::to('news/'.$popular_news->slug)}}" class="">
                        <strong>{{$popular_news->title}}</strong>
                    </a>

                    <p class="mt10 mb5">
                        {{ substr(strip_tags($popular_news->description),0,180) }}..
                    </p>

                    <small class="text-primary">{{ $popular_news->created_at->format('d F Y') }}</small>
                    <hr>
                </div>
                @endforeach
            </div>




            <div class="pr25 pl25 pb25 clearfix">
                <h5 class="mt25 text-primary">
                    <b>Archives</b>
                    <span class="heading-divider mt10"></span>
                </h5>
                <div class="form-group">
                    <?php
                        $archives = \App\News::selectRaw('COUNT(id) as post_count, YEAR(created_at) year, MONTHNAME(created_at) month')->groupBy('year', 'month')->orderByRaw('min(created_at) DESC')->get();
                        ?>
                        <select id="archives" onchange="javascript:location.href = this.value;">
                            @foreach ($archives as $archive)
                            <option value="{{ URL::to('archive?month=' . $archive->month . '&year=' . $archive->year) }}">{{ $archive->month }} {{ $archive->year }} ({{ $archive->post_count }})</option>
                            @endforeach
                        </select>
                </div>
            </div>
        </div>

    </div>

    <!-- Popular Products
        ===================================== -->
    <div class="panel p30" style="background-image: url('https://www.monexsecurities.com.au/uploads/images/design/bg2.png')">
        <div class="form-group text-center ">
            <h3 class="color-light">
                <b>FREE EBOOK!</b>
            </h3>
            <p class="color-dark">Find out how easy it is to start investing in international stocks at a low cost.</p>
        </div>
        <div class="form-group text-center">
            <input class="input-md input-circle form-control bg-green" type="text" name="fullname" placeholder="Full name" value="">
        </div>
        <div class="form-group text-center">
            <input class="input-md input-circle form-control bg-green" type="email" name="email" placeholder="Email" value="" å>
        </div>
        <div class="form-group text-center mt50">
            <button class="button-3d btn-block button-md button-circle button-green ">GET YOUR FREE BOOK</button>
        </div>
    </div>



    {{--
    <div class="mt25 pr25 pl25 clearfix">
        <h5 class="mt25">
            Popular Post
            <span class="heading-divider mt10"></span>
        </h5>
        @foreach(\App\News::where('status','1')->orderBy('views','desc')->take(getcong_widgets('popular_posts_limit'))->get() as
        $popular_news)
        <div class="blog-sidebar-popular-post-container">
            <img src="{{ URL::asset('upload/news/'.$popular_news->image.'-s.jpg') }}" alt="" class="img-responsive pull-left">
            <h6>
                <a href="{{URL::to('news/'.$popular_news->slug)}}">{{$popular_news->title}}</a>
            </h6>
            <span class="text-gray">{{date('m/d/Y',strtotime($popular_news->created_at))}}</span>
        </div>
        @endforeach

    </div> --}}


    <!-- Archieve
        ===================================== -->
    {{--
    <div class="mt25 pr25 pl25 clearfix">
        <h5 class="mt25">
            Archieve
            <span class="heading-divider mt10"></span>
        </h5>
        <ul class="shop-sidebar pl25">
            <li class="active">
                <a href="#">January
                    <span class="badge badge-pasific pull-right">14</span>
                </a>
            </li>
            <li>
                <a href="#">February
                    <span class="badge badge-pasific pull-right">125</span>
                </a>
            </li>
            <li>
                <a href="#">March
                    <span class="badge badge-pasific pull-right">350</span>
                </a>
            </li>
            <li>
                <a href="#">April
                    <span class="badge badge-pasific pull-right">520</span>
                </a>
            </li>
            <li>
                <a href="#">May
                    <span class="badge badge-pasific pull-right">1,290</span>
                </a>
            </li>
            <li>
                <a href="#">June
                    <span class="badge badge-pasific pull-right">7</span>
                </a>
            </li>
        </ul>

    </div> --}}


</div>